@extends('layouts.app')
@section('content')
    <center><h3 class="mt-3 alert alert-info text-secondary" role="alert">LIST URL</h3></center>
    <a href="{{url('/new')}}"><button type="button" id="button-addon2" class="btn btn-info mb-4">CREATE URL</button></a>

    @if(count($shorts)>0)
        @foreach($shorts as $short)
            <div>
                <a href="{{url($short->longURL)}}">
                    <h5 class="text-warning">{{$short->longURL}}</h5>
                </a>

                    <div class="row">
                        <div class="col">
                            <p class="text-muted">{{$short->created_at}}</p>
                        </div>
                        <div class="col">
                            <b><p class="text-right text-info">view : {{$short->view}}</p></b>
                        </div>
                    </div>


                <div class="input-group mb-4">
                    <input id="shortenurl{{$short->id}}" class="form-control text-info outline-info" type="text"  value="http://www.short.local/t/{{$short->shortURL}}" readonly>
                        <div class="input-group-append">
                            <button onclick="copy(this)" value="{{$short->id}}" type="button" id="button-addon2" class="btn btn-outline-info">COPY</button>
                        </div>
                </div>

            </div>
            <hr>
            @endforeach
        @endif

        <script>
            function copy(clickedBtn){
            var id = clickedBtn.value;
            var copyText = document.querySelector('#shortenurl'+id);
            copyText.select();
            document.execCommand('copy');
            alert('Copied ' + copyText.value);
            }
        </script>



@endsection

