@extends('layouts.app')
@section('content')

    <center><h3 class="mt-3 alert alert-info text-secondary" role="alert">CREATE URL</h3></center>
    <a href="{{url('/')}}"><button type="button" id="button-addon2" class="btn btn-info mb-4">LIST URL</button></a>
    <form method="post" action="{{ url('/') }}">
        @csrf

        <div class="input-group mb-4">
            <input type="text" name="longURL" class="form-control text-info outline-info">
            <div class="input-group-append">
                <button class="btn btn-outline-info" type="submit">CREATE SHORT URL</button>
            </div>
        </div>

    </form>
@endsection
