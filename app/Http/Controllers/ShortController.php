<?php

namespace App\Http\Controllers;
use App\Short;
use Illuminate\Http\Request;

class ShortController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $shorts = Short::OrderBy('created_at','desc')->get();
        return view('index')->with('shorts',$shorts);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $charset = "abcdefghijklmnopqrstuvwxyz";
        $numset = "0123456789";
        $url = "";
        for($i=0; $i<5; $i++)
        {
            $url .= $numset[rand(0,strlen($numset))-1];
            //$url = $url . $numset[rand(0,9)];
        }
        $url .= $charset[rand(0,strlen($charset))-1];
        //$url .= $charset[rand(0,25)];
        $this->validate($request,
            [
                'longURL' => 'required',
            ]
        );

        $short = new Short();
        $short->longURL = $request->input('longURL');
        $short->shortURL = $url;
        $short->view = 0;
        $short->save();

        return redirect('new')->with('success','success! http://www.short.local/t/'.$short->shortURL);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($shorten)
    {
        //
        $shorts = Short::all();
        if(count($shorts)>0){
            foreach ($shorts as $short){
                if ($short->shortURL == $shorten){
                    $short->view += 1;
                    $short->save();
                    return view('redirect')->with('longURL',$short->longURL);
                }
            }
        }
        return view('notfound');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
